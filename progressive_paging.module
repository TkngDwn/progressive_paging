<?php

/**
 * @file
 * Provides progressive paging capability to Drupal contents.
 */

define('PROGRESSIVE_PAGING_MAX_CHAR_LIMIT', 3072);
define('PROGRESSIVE_PAGING_MAX_WORD_LIMIT', 512);

/**
 * Implements hook_node_view().
 */
function progressive_paging_node_view($node, $view_mode, $langcode) {
  if (($node->type == 'article' || $node->type == 'book') && $view_mode == 'full') {
    // Make sure the canonical tag is present.
    $page_link = url(current_path(), array('absolute' => TRUE));
    drupal_add_html_head(array(
      '#attributes' => array(
        'rel'  => 'canonical',
        'href' => $page_link,
      ),
      '#tag' => 'link',
      '#attached' => array(
        'drupal_add_http_header' => array(array('Link', '<' . $page_link . '>; rel="canonical"', 1)),
      ),
    ), 'progressive_paging_link_canonical');

    // Start processing the body field.
    $total_page = $output_index = $current_page = $word_count = 0;
    $path = current_path();
    $current_page = (isset($_GET['page'])) ? $_GET['page'] : 0;
    $placeholder = '<!--pagebreak-->'; // The drupal default pagebreak placeholder.
    $markup = $node->content['body'][0]['#markup'];

    // If the placeholder is present we will attempt to split the pages on it, otherwise we will fallback to the word count method.
    if (strpos($markup, $placeholder) === FALSE) {
      $split_tags = preg_split('/<(!--.*?--|[^>]+?)>/s', $markup, -1, PREG_SPLIT_DELIM_CAPTURE);
      $markup = '';
      foreach ($split_tags as $split_tags_key => $split_tags_value) {
        if ($split_tags_key & 1) {
          $markup .= '<' . $split_tags_value . '>';
          // Keep track of block level tags
          if (!isset($block_tag_value)) {
            $block_tag_value = (strpos($split_tags_value, ' ') !== FALSE) ? strstr($split_tags_value, ' ', TRUE) : $split_tags_value;
            $block_tag_occurrences = 0;
          } else {
            // Keep track of additional occurrences of the block level tag
            if (((strpos($split_tags_value, ' ') !== FALSE) ? strstr($split_tags_value, ' ', TRUE) : $split_tags_value) == $block_tag_value) {
              $block_tag_occurrences++;
            } else {
              // If the tag is a closing tag and we have occurrences then deincrement
              if ($split_tags_value == '/'.$block_tag_value) {
                if ($block_tag_occurrences) {
                  $block_tag_occurrences--;
                } else {
                  unset($block_tag_value);
                  if ($word_count >= PROGRESSIVE_PAGING_MAX_CHAR_LIMIT) {
                    $markup .= $placeholder;
                    $word_count = 0;
                  }
                }
              }
            }
          }
        } else {
          $split_text = explode(' ', $split_tags_value);
          $split_text_size = count($split_text);
          foreach ($split_text as $text_value) {
            // Exclude whitespace
            if (preg_match('/\S/s', $text_value)) {
              $word_count = $word_count + strlen($text_value);
            }
            $markup .= $text_value;
            if ($split_text_size > 1) {
              $markup .= ' ';
            }
          }
        }
      }
    }
    do {
      // Look for consecutive placeholder and closing tag. Eg. <!--pagebreak--></p>
      $placeholder_regex = preg_quote($placeholder, '#');
      preg_match_all("#($placeholder_regex)([ \t\r\n]*)(</[a-zA-Z]+>)#", $markup, $match);
      if (isset($match[1][0]) && isset($match[3][0])) {
        // Move the placeholder to the end of closing tag. From the above example </p><!--pagebreak-->
        $match_regex = preg_quote($match[1][0] . (isset($match[2][0]) ? $match[2][0] : '') . $match[3][0], '#');
        $markup = preg_replace("#$match_regex#", (isset($match[2][0]) ? $match[2][0] : '') . $match[3][0] . $match[1][0], $markup);
      }
    } while (!empty($match[0]) && !empty($match[1]) && !empty($match[2]) && !empty($match[3]));

    // Break HTML content properly and insert placeholder
    $markup = smart_page_break_insert_placeholder($markup);

    // Check if last page is an empty tag
    $pagebreak = array_filter(explode($placeholder, $markup));
    $pagebreak_end = count($pagebreak) - 1;
    $last_page = '';
    if (isset($pagebreak[$pagebreak_end])) {
      $last_page = strip_tags($pagebreak[$pagebreak_end]);
    }
    if (empty($last_page)) {
      // Remove the last page with only an empty tag content
      unset($pagebreak[$pagebreak_end]);
    }

    // Set the body content.
    $markup = implode($placeholder, $pagebreak);
    if (empty($pagebreak)) {
      // Break the content based on user's included page break placeholders
      $pagebreak = array_filter(explode($placeholder, $markup));
    }
    $pagebreak_count = count($pagebreak);
    if ($pagebreak_count > 1) {
      $output = '';
      if (isset($pagebreak[$current_page])) {
        $output = $pagebreak[$current_page];
      }
      // Record total number of pages
      $total_page += count($pagebreak);
      // Delivers a "page not found" error to the browser if desired in setting for non valid pages.
      if ($current_page >= $total_page) {
        drupal_not_found();
      }
      // Final field content output
      $node->content['body'][0]['#markup'] = $output;
    }

    if (!empty($total_page)) {
      global $base_url;
      $current_url = function_exists("path_alias_xt_get_path_alias") ? path_alias_xt_get_path_alias($path) : drupal_get_path_alias($path);
      $total_page_offset = $total_page - 1;
      
      // Pagination with rel="next" and rel="prev"
      // Ref: http://googlewebmastercentral.blogspot.co.uk/2011/09/pagination-with-relnext-and-relprev.html
      if ($current_page == 0) {
        // We are on first page
      } else if ($current_page == $total_page_offset) {
        // We are on last page
        $link_rel_prev_page = $total_page_offset - 1;
      } else {
        // We are on middle page
        $link_rel_next_page = $current_page + 1;
        $link_rel_prev_page = $current_page - 1;
      }
      if (isset($link_rel_prev_page)) {
        $href = $link_rel_prev_page ? "$base_url/$current_url?$link_rel_prev_page" : "$base_url/$current_url";
        drupal_add_html_head(array(
          '#attributes' => array(
            'rel'  => 'prev',
            'href' => $href,
          ),
          '#tag' => 'link'
        ), 'progressive_paging_link_rel_prev');
      }
      if (isset($link_rel_next_page)) {
        $href = $link_rel_next_page ? "$base_url/$current_url?$link_rel_next_page" : "$base_url/$current_url";
        drupal_add_html_head(array(
          '#attributes' => array(
            'rel'  => 'next',
            'href' => $href,
          ),
          '#tag' => 'link'
        ), 'progressive_paging_link_rel_next');
      }

      // A very hacky way to add the pager to nodes, but we know nothing else on these pages will use pagination.
      global $pager_page_array, $pager_total;
      $pager_page_array[0] = $current_page;
      $pager_total[0] = $total_page;
      $variables['tags'] = array();
      $variables['quantity'] = 10;
      $node->content['body'][0]['#markup'] .= '<div class="smart-paging-pager">' . theme('pager', $variables) . '</div>';
    }
  }
}

/**
 * Break HTML content and insert placeholder
 *
 * Helper function to properly break HTML content and insert placeholder.
 *
 * @param $text
 *   HTML text content.
 * @param $placeholder
 *   Placeholder string to be inserted between broken page.
 * @return
 *   HTML output of properly break HTML content and insert placeholder.
 */
function smart_page_break_insert_placeholder($text, $placeholder = NULL) {
  if (empty($placeholder)) {
    $placeholder = '<!--pagebreak-->'; // The default drupal pagebreak placeholder.
  }
  // Perform HTML correction
  $text = _filter_htmlcorrector($text);
  $break_map = array();
  $text = str_replace($placeholder, '<progressive_paging_placeholder>', $text) . '<progressive_paging_placeholder>';
  $struct = smart_page_pair_tags($text);
  $split = $struct['split'];
  $tag_pairs_map = $struct['tag_pairs_map'];
  $break_positions = array_keys($tag_pairs_map['progressive_paging_placeholder']);
  unset($tag_pairs_map['progressive_paging_placeholder']);
  foreach ($break_positions as $break_index => $break_end) {
    if ($break_index) {
      $break_start = $break_positions[$break_index - 1];
      $break_map[$break_end] = array_slice($split, $break_start, ($break_end - $break_start), TRUE);
      // Remove the placeholder from the structure 
      unset($break_map[$break_end][$break_start]);
    } else {
      $break_start = $break_index;
      $break_map[$break_end] = array_slice($split, $break_start, ($break_end - $break_start), TRUE);
    }
    foreach ($tag_pairs_map as $tag_name => $map) {
      foreach ($map as $open_tag_pos => $close_tag_pos) {
        if (($open_tag_pos < $break_end && $close_tag_pos > $break_end) || ($open_tag_pos < $break_start && $close_tag_pos > $break_start) || ($open_tag_pos > $break_start && $close_tag_pos < $break_end && $close_tag_pos !== NULL) || ($open_tag_pos > $break_start && $open_tag_pos < $break_end && $close_tag_pos === NULL)) {
          if (empty($close_tag_pos)) {
            $break_map[$break_end][$open_tag_pos] = '<' . $split[$open_tag_pos] . '>';
          } else {
            $break_map[$break_end][$open_tag_pos]  = '<' . $split[$open_tag_pos] . '>';
            $break_map[$break_end][$close_tag_pos] = '<' . $split[$close_tag_pos] . '>';
          }
        }
      }
    }
    ksort($break_map[$break_end]);
    if (isset($output)) {
      $output .= $placeholder . implode('', $break_map[$break_end]);
    } else {
      $output = implode('', $break_map[$break_end]);
    }
  }
  return $output;
}

/**
 * Create page content HTML paired tags structure
 *
 * Helper function to create a structured HTML paired tags mappings.
 *
 * @param $text
 *   HTML text content.
 * @return
 *   An associative array containing:
 *   - split: array of split HTML tag(s) from text.
 *   - tag_pairs_map: Array of structured HTML paired tags mapping:
 *   $array[<tag name>][<open tag position>] = <close tag position>
 */
function smart_page_pair_tags($text) {
  $tag_pairs_map = array();
  // Properly entify angles.
  $text = preg_replace('@<(?=[^a-zA-Z!/]|$)@', '&lt;', $text);
  // Split tags from text.
  $split = preg_split('/<(!--.*?--|[^>]+?)>/s', $text, -1, PREG_SPLIT_DELIM_CAPTURE);
  // Note: PHP ensures the array consists of alternating delimiters and literals
  // and begins and ends with a literal (inserting null as required).
  foreach ($split as $position => $value) {
    // Tags are in array's odd number index
    if ($position & 1) {
      list($tagname) = explode(' ', $value);
      if ($tagname[0] == '/') {
        $tagname = strtolower(substr($tagname, 1));
        end($tag_pairs_map[$tagname]);
        // Its open tag pair is the last item in the array with empty value
        while ($tag_value = current($tag_pairs_map[$tagname])) {
          prev($tag_pairs_map[$tagname]);
        }
        $pair_pos = key($tag_pairs_map[$tagname]);
        // Save position of closing tag to its pair open tag
        $tag_pairs_map[$tagname][$pair_pos] = $position;
      } else {
        $tagname = strtolower($tagname);
        // Save position of open tag. For now, closing tag is still unidentified
        $tag_pairs_map[$tagname][$position] = NULL;
      }
    }
  }

  return array(
    'split' => $split,
    'tag_pairs_map' => $tag_pairs_map,
  );
}

